package main

import (
	"log"
	books "pustaka-api/Books"
	handler "pustaka-api/Handler"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {

	dsn := "root:@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Gagal koneksi ke database")
	}

	db.AutoMigrate(&books.Book{})

	bookRepository := books.NewRepository(db)
	bookService := books.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)

	router := gin.Default()
	router.GET("/", bookHandler.HandleStatus)
	router.GET("/books", bookHandler.HandleGetBooks)
	router.GET("/book/:id", bookHandler.HandleGetBookById)
	router.POST("/book", bookHandler.HandlePostBook)
	router.PUT("/book/:id", bookHandler.HandleUpdateBookById)
	router.DELETE("/book/:id", bookHandler.HandleDeleteBook)

	router.Run()
}
