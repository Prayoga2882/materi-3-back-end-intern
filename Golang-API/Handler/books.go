package handler

import (
	"fmt"
	"log"
	"net/http"
	books "pustaka-api/Books"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type bookHandler struct {
	bookService books.Service
}

func NewBookHandler(bookService books.Service) *bookHandler {
	return &bookHandler{bookService}
}

func(h *bookHandler) HandleStatus(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"Status": "OK",
	})
}

func (h *bookHandler) HandleGetBooks(ctx *gin.Context){
	books, err := h.bookService.FindAll()
	if err != nil {	
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error" : err,
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"Successfully" : books,
	})
}

func(h *bookHandler) HandleGetBookById(ctx *gin.Context) {
	idString := ctx.Param("id")
	id, _ := strconv.Atoi(idString)
	
	book, err := h.bookService.FindById(int(id))
	log.Println(err)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error" : "data tidak ada di database",
		})
		return 
	}

	ctx.JSON(http.StatusOK, gin.H{
		"Succesfully" : book,
	})
}

func(h *bookHandler) HandlePostBook(ctx *gin.Context){
	var bookRequest books.BookInput
	err := ctx.ShouldBindJSON(&bookRequest)
	
	if err != nil {
		errorMessages := []string{"Input tidak memenuhi syarat"}

		ctx.JSON(http.StatusBadRequest, gin.H{
			"errors" : errorMessages,
		})
		return
	}

	book, err := h.bookService.Create(bookRequest)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errors" : err,
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"Successfully" : book,
	})
}

func(h *bookHandler) HandleUpdateBookById(ctx *gin.Context){
	var bookRequest books.BookInput
	err := ctx.ShouldBindJSON(&bookRequest)

	if err != nil {
		errorMessages := []string{}
			for _, e := range err.(validator.ValidationErrors){
				errorMessage := fmt.Sprintf("Error pada field %s karena statement is %s", e.Field(), e.ActualTag())
				errorMessages = append(errorMessages, errorMessage)
			}

		ctx.JSON(http.StatusBadRequest, gin.H{
			"errors" : errorMessages,
		})
		return
	}

	idString := ctx.Param("id")
	id, _ := strconv.Atoi(idString)

	book, err := h.bookService.Update(id, bookRequest)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errors" : err,
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"Successfully" : book,
	})
}

func(h *bookHandler) HandleDeleteBook(ctx *gin.Context) {
	idString := ctx.Param("id")
	id, _ := strconv.Atoi(idString)
	
	book, err := h.bookService.Delete(int(id))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error" : err,
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"Succesfully" : book,
	})
}