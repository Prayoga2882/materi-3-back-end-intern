package books

import "log"

type Service interface {
	FindAll() ([]Book, error)
	FindById(ID int) (Book, error)
	Create(bookRequest BookInput) (Book, error)
	Update(ID int, bookRequest BookInput) (Book, error)
	Delete(ID int) (Book, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]Book, error) {
	books, err := s.repository.FindAll()
	return books, err
	// return s.repository.FindAll()
}

func (s *service) FindById(ID int) (Book, error) {
	book, err := s.repository.FindById(ID)
	return book, err

	// return s.repository.FindById()
}

func (s *service) Create(bookRequest BookInput) (Book, error) {
	Qty, _ := bookRequest.Qty.Int64()
	Price, _ := bookRequest.Price.Float64()
	book := Book{
		Title:       bookRequest.Title,
		Description: bookRequest.Description,
		Qty:         int(Qty),
		Price:       int(Price),
	}
	log.Println("Proses service")
	newBook, err := s.repository.Create(book)
	return newBook, err
	// return s.repository.Create()
}

func (s *service) Update(ID int, bookRequest BookInput) (Book, error) {
	book, err := s.repository.FindById(ID)

	Qty, _ := bookRequest.Qty.Int64()
	Price, _ := bookRequest.Price.Float64()

	book.Title = bookRequest.Title
	book.Description = bookRequest.Description
	book.Qty = int(Qty)
	book.Price = int(Price)

	newBook, err := s.repository.Update(book)
	return newBook, err
	// return s.repository.Update()
}

func (s *service) Delete(ID int) (Book, error) {
	book, err := s.repository.FindById(ID)

	newBook, err := s.repository.Delete(book)
	return newBook, err
	// return s.repository.Update()
}