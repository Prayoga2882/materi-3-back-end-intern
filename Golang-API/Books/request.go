package books

import "encoding/json"

type BookInput struct {
	Title string      `json:"Title" binding:"required"`
	Description string `json:"Description" binding:"required"`
	Qty   json.Number `json:"Qty" binding:"required"`
	Price json.Number `json:"Price" binding:"required"`
}
