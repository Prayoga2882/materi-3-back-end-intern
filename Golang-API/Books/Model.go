package books

import "time"

type Book struct {
	ID          int
	Title       string
	Description string
	Price       int
	Qty			int
	CreatedAt   time.Time
	UpdatedAt	time.Time
}