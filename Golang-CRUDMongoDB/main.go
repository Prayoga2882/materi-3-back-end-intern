package main

import (
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"main/helper"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var client *mongo.Client

func GetConnection() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, _ = mongo.Connect(ctx, clientOptions)

}

type Books struct {
	Id     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Cover  string             `json:"cover,omitempty" bson:"cover,omitempty"`
	Writer string             `json:"writer,omitempty" bson:"writer,omitempty"`
	Price  int                `json:"price,omitempty" bson:"price,omitempty"`
}

func createBook(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	var book Books
	err := json.NewDecoder(request.Body).Decode(&book)
	helper.IfError(err)
	collection := client.Database("GolangAPI").Collection("golangapi")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, _ := collection.InsertOne(ctx, book)
	err = json.NewEncoder(response).Encode(result)
	helper.IfError(err)
}
func getBooks(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	collection := client.Database("GolangAPI").Collection("golangapi")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	rows, err := collection.Find(ctx, bson.M{})
	helper.IfError(err)
	defer rows.Close(ctx)
	var people []Books
	for rows.Next(ctx) {
		var person Books
		rows.Decode(&person)
		people = append(people, person)
	}
	err = json.NewEncoder(response).Encode(people)
	helper.IfError(err)
}
func getBook(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	params := mux.Vars(request)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	var book Books
	collection := client.Database("GolangAPI").Collection("golangapi")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	collection.FindOne(ctx, Books{Id: id}).Decode(&book)
	err := json.NewEncoder(response).Encode(book)
	helper.IfError(err)
}

func updateBook(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()
	coll := client.Database("GolangAPI").Collection("golangapi")
	var params = mux.Vars(request)
	id, _ := primitive.ObjectIDFromHex(params["id"])

	opts := options.Update().SetUpsert(true)
	filter := bson.D{{"_id", id}}
	update := bson.D{{"$set", bson.D{
		{"cover", "Stip & pensil"},
		{"writer", "joko"},
		{"price", 1000},
	}}}

	result, err := coll.UpdateOne(context.TODO(), filter, update, opts)
	helper.IfError(err)

	if result.MatchedCount != 0 {
		fmt.Println("Successfully Update Collection Document")
		return
	}
	err = json.NewEncoder(response).Encode(result)
	helper.IfError(err)

}
func deleteBook(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	params := mux.Vars(request)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	var book Books
	collection := client.Database("GolangAPI").Collection("golangapi")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	collection.FindOneAndDelete(ctx, Books{Id: id}).Decode(&book)
	err := json.NewEncoder(response).Encode(book)
	helper.IfError(err)
}

func main() {
	GetConnection()
	fmt.Println("Starting the application...")
	router := mux.NewRouter()

	// Route handles & endpoints
	router.HandleFunc("/", getBooks).Methods("GET")
	router.HandleFunc("/GetBooks", getBooks).Methods("GET")
	router.HandleFunc("/GetBookById/{id}", getBook).Methods("GET")
	router.HandleFunc("/CreateBook", createBook).Methods("POST")
	router.HandleFunc("/UpdateBook/{id}", updateBook).Methods("PUT")
	router.HandleFunc("/DeleteBook/{id}", deleteBook).Methods("DELETE")

	err := http.ListenAndServe(":8080", router)
	helper.IfError(err)

}
