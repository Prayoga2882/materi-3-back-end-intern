package helper

func IfError(err error) {
	if err != nil {
		panic(err)
	}
}

type ErrorResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

func NewErrorResponse(err error) ErrorResponse {
	return ErrorResponse{
		Status:  "Error",
		Message: err.Error(),
	}
}
